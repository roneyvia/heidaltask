import { Component, OnInit } from '@angular/core';
import { GetOperatorsService } from '../service/get-operators.service';

@Component({
  selector: 'app-cost-finder',
  templateUrl: './cost-finder.component.html',
  styleUrls: ['./cost-finder.component.scss']
})

export class CostFinderComponent implements OnInit {
  finalVal: any;
  isDataAvailable: boolean;
  operations: any;
  phone: string;
  maxPrefix: any;
  doPrefix: any;
  doList: any;
  operators: any;
  bestOperator: any;

  constructor(private operation: GetOperatorsService) { }

  ngOnInit() {
    this.getOperatorsInfo();
    this.isDataAvailable = undefined;
  }

  getOperatorsInfo() {
    this.operation.getOperations().subscribe((res) => {
      this.operators = res;
      this.maxPrefix = this.operators.maxPrefix;
    });
  }

  check = (doPref, prefList, operator) => {
    for (let i = 0; i < prefList.length; i++) {
      if (doPref == prefList[i].prefix) {
        prefList[i].operator = operator;
        this.doList.push(prefList[i]);
        return true;
      }
    }

  }

  // tslint:disable-next-line:no-unused-expression
  getBestOperator = () => {
    this.doList = [];
    this.isDataAvailable = undefined;
    const phoneNumber = this.phone.replace(/[^0-9]/g, '');
    this.doPrefix = phoneNumber.slice(0, this.maxPrefix);
    for (let i = 0; i < this.operators.operators.length; i++) {
      let curPref = this.maxPrefix;
      for (let j = 0; j < this.operators.operators[i].list.length; j++) {
        // tslint:disable-next-line:max-line-length
        if (this.check(this.doPrefix.slice(0, curPref--), this.operators.operators[i].list, this.operators.operators[i].operator)) { // repeat

          break;
        }
      }
    }
    this.doList.sort((a, b) => (a.prefix > b.prefix) ? -1 : (a.prefix === b.prefix) ? ((a.cost < b.cost) ? -1 : 1) : 1);
    this.doList.length > 0 ? this.bestOperator = this.doList[0] : this.bestOperator = [];
    this.bestOperator.length === 0 ? this.isDataAvailable = false : this.isDataAvailable = true;
  }

}
