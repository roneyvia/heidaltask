import { TestBed } from '@angular/core/testing';

import { GetOperatorsService } from './get-operators.service';

describe('GetOperatorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetOperatorsService = TestBed.get(GetOperatorsService);
    expect(service).toBeTruthy();
  });
});
