import { Injectable } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetOperatorsService {

  constructor(private http: HttpClient) { }

  getOperations(): Observable<any> {
    return this.http.get('assets/operators.json')
      .pipe(map(res => res));

  }
}
