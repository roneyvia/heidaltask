import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CostFinderComponent } from './cost-finder/cost-finder.component';

import { GetOperatorsService } from './service/get-operators.service';

@NgModule({
  declarations: [
    AppComponent,
    CostFinderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [GetOperatorsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
